//index.js
Page({
  data: {
    list: [],
    complete: 0,
    inputValue: '',
  },

  bindKeyInput: function(e) {
    this.setData({
      inputValue: e.detail.value,
    })
  },

  confirmAdd: function() {
    const list = this.data.list;
    const inputVal = this.data.inputValue;
    if (inputVal == '') {
      wx.showToast({
        title: '输入不能为空',
        icon: 'none',
      })
    } else {
      list.push({ checked: false, value: inputVal });
      this.setData({
        list,
        inputValue: '',
      });
    }
  },

  delete: function(e) {
    const list = this.data.list;
    list.splice(e.target.dataset.index,1);
    if(list.length === 0) {
      wx.showToast({
        title: '任务列表为空',
        icon: 'none',
      })
    }
    this.setData({
      list,
    })
    this._completeTask()
  },

  change: function(e) {
    const list = this.data.list;
    list[e.target.dataset.index].checked = !!e.detail.value[0];
    this.setData({
      list,
    })
    this._completeTask();
  },

  _completeTask: function() {
    let complete = 0, list = this.data.list;
    for(let i = 0, len = list.length; i < len; i++){
      if(!!list[i].checked){
        complete++;
      }
    }
    this.setData({
      complete: complete,
    })
  }
})
